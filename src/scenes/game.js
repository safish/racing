import core, {monitor, Tilemap} from '../core'
import {Gamepad} from '../components'

export default {
    container: new PIXI.Container(),

    create() {
        this.setup()
        core.stage.addChild(this.container)
        core.ticker.add(this.update, this)
    },

    setup() {
        this.track = PIXI.Sprite.from('static/maps/1.png')
        this.car = PIXI.Sprite.fromFrame('car.black.1.png')
        this.gamepad = new Gamepad()
        this.map = new Tilemap(core.loader.resources.map.data, PIXI.Texture.from('tilesheet'))
        this.container.addChild(this.map, this.gamepad)
    },

    update() {
        // this.map.y--
    }
}