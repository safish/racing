import core, {monitor} from '../core'
import config from '../config'
import {tween, easing} from 'popmotion'
import {api} from '../components'

core.loader.baseUrl = config.cdn


export default {
    container: new PIXI.Container(),

    preload() {
        core.loader
            .add('misc', `static/textures/misc.json`)
            .add('tilesheet', 'static/textures/sprites.png')
            .add('map', 'static/maps/2.json')
            .load(this.create.bind(this))
    },

    create() {
        monitor.emit('scene:game')
    }
}