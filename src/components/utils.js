export default {
    get isWechat() {
        return typeof wx !== 'undefined'
    },

    getValue(a, b, c) {
        return a === b ? c : a
    }
}
