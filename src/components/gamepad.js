import config from '../config'
import {tween} from 'popmotion'
import core, {monitor} from '../core'

export default class Gamepad extends PIXI.Container {
    constructor() {
        super()
        // this.scale.set(config.zoom)
        this.addTrackball()
        this.addKeyA()
        this.listen()
    }

    addTrackball() {
        const area = new PIXI.Graphics()

        area.beginFill(0x00bcd4, .3)
        area.drawCircle(0, 0, 150)
        area.endFill()
        this.trackball = new PIXI.Graphics()
        this.trackball.beginFill(0xffffff, .3)
        this.trackball.drawCircle(0, 0, 120)
        this.trackball.endFill()

        area.addChild(this.trackball)
        this.addChild(area)
    }

    addKeyA() {
        const text = new PIXI.Text('A', {
            fill: 0xffffff,
            fontSize: 60
        })
        text.pivot.set(text.width * .5, text.height * .5)
        this.keyA = new PIXI.Graphics()
        this.keyA.beginFill(0x4caf50, .5)
        this.keyA.drawCircle(0, 0, 80)
        this.keyA.endFill()
        this.keyA.addChild(text)
        this.addChild(this.keyA)
    }

    pointerup() {
        this.trackball.down = false
        this.trackball.touchId = null
        tween({
            from: {x: this.trackball.x, y: this.trackball.y},
            to: {x: 0, y: 0},
            duration: 100
        }).start(v => {
            this.trackball.position.set(v.x, v.y)
        })
        monitor.emit('player:stand')
    }

    listen() {
        this.trackball.interactive = true
        this.trackball
            .on('pointerdown', ev => {
                this.trackball.down = true
                this.trackball.touchId = ev.data.identifier
                this.trackball.dx = ev.data.global.x - this.trackball.x
                this.trackball.dy = ev.data.global.y - this.trackball.y
            })
            .on('pointermove', ev => {
                if (!this.trackball.down || this.trackball.touchId !== ev.data.identifier) return
                this.trackball.x = ev.data.global.x - this.trackball.dx
                this.trackball.y = ev.data.global.y - this.trackball.dy
                const
                    angle = Math.atan2(this.trackball.y, this.trackball.x),
                    dr = Math.sqrt(this.trackball.x ** 2 + this.trackball.y ** 2) - 30

                if (dr > 0) {
                    this.trackball.x -= Math.cos(angle) * dr
                    this.trackball.y -= Math.sin(angle) * dr
                }

                this.trackball.x > 0 ? monitor.emit('player:right') :
                this.trackball.x < 0 ? monitor.emit('player:left') : null

            })
            .on('pointerup', this.pointerup.bind(this))
            .on('pointerupoutside', this.pointerup.bind(this))

        this.keyA.interactive = true
        this.keyA.finished = true
        this.keyA.on('pointerdown', () => {
            this.keyA.tween = this.keyA.tween || tween({
                from: 1,
                to: 1.2,
                duration: 100,
                yoyo: true
            })

            monitor.emit('player:jump')

            if (this.keyA.finished) {
                this.keyA.finished = false
                this.keyA.tween.start({
                    update: v => this.keyA.scale.set(v),
                    complete: () => this.keyA.finished = true
                })
            }
        })

        // 绝对定位
        this.on('added', () => {
            let rect = new PIXI.Rectangle()
            this.trackball.parent.getBounds(false, rect)
            this.trackball.parent.position.set(
                (100 - rect.x) / core.stage.scale.x,
                (config.screen.height - rect.bottom - 100) / core.stage.scale.y
            )

            this.keyA.getBounds(false, rect)
            this.keyA.position.set(
                (config.screen.width - rect.right - 100) / core.stage.scale.x,
                (config.screen.height - rect.bottom - 150) / core.stage.scale.y
            )
        })
    }
}