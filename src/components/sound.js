import {monitor} from '../core'
import config from '../config'

class Sound {
    constructor(option={}) {
        const ctx = wx.createInnerAudioContext()
        ctx.autoplay = option.autoplay
        ctx.src = `${config.cdn}/${option.src}`
        ctx.volume = option.volume

        if (option.autoDestroy !== false) {
            ctx.onStop(() => {
                ctx.destroy()
            })

            ctx.onEnded(() => {
                ctx.destroy()
            })
        }

    }
}


monitor
    .on('sound:bgm:play', () => {
        // new Sound({src: 'static/sounds/bgm.mp3', autoplay: true, volume: .2})
    })
    .on('sound:select:play', i => {
        i > 9 ? i = 9 : null
        if (typeof wx !== 'undefined') {
            new Sound({src: `static/sounds/select.${i}.mp3`, autoplay: true, volume: .3})
        } else {
            const sound = core.loader.resources[`sound:select:${i}`].sound
            sound.volume = .3
            sound.play()
        }

    })
    .on('sound:erase:play', i => {
        if (typeof wx !== 'undefined') {
            new Sound({src: 'static/sounds/erase.mp3', autoplay: true, volume: .05})
        } else {
            const sound = core.loader.resources['sound:erase'].sound
            sound.volume = .05
            sound.play()
        }
    })