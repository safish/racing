export default {
    cdn     : web ? '.' : location.origin ? `${location.origin}/game` : 'https://wisdom.lufei.so/donut',
    bgColor : 0xa1d6e7,
    mode    : 'landscape',
    zoom    : 1,
    debug   : true, // 正式版务必关掉
    screen  : {
        width: 750,
        height: 1334,
        orientation: 0,
        resolution: 1,
        get ratio() {return this.width / this.height}
    },
    design  : {
        width: 1334,
        height: 750,
        mode: 'landscape',
        get ratio() {return this.width / this.height}
    }
}