import config from '../config'
import utils from '../components/utils'
import * as PIXI from 'pixi.js'


let pixelRatio, windowHeight, windowWidth

if (utils.isWechat) {
    ({pixelRatio, windowHeight, windowWidth} = wx.getSystemInfoSync())

    PIXI.utils.isWebGLSupported = () => {
        return canvas.getContext('webgl')
    }

    PIXI.interaction.InteractionManager.prototype.mapPositionToPoint = (point, x, y) => {
        point.x = x * pixelRatio
        point.y = y * pixelRatio
    }
} else {
    pixelRatio = window.devicePixelRatio
    windowWidth = window.innerWidth
    windowHeight = window.innerHeight
    window.canvas = document.querySelector('canvas')

    if (window.innerHeight > window.innerWidth && config.mode === 'landscape') {
        windowWidth = window.innerHeight
        windowHeight = window.innerWidth

        canvas.style.transform = 'rotate(90deg)'
        canvas.style.width = `${windowWidth}px`
        canvas.style.height = `${windowHeight}px`
        canvas.style.top = `${(innerHeight - windowHeight) * .5}px`
        canvas.style.left = `${(innerWidth - windowWidth) * .5}px`
    }

    if (window.innerHeight < window.innerWidth && config.mode === 'portrait') {
        windowWidth = window.innerHeight
        windowHeight = window.innerWidth

        canvas.style.transform = 'rotate(90deg)'
        canvas.style.width = `${windowWidth}px`
        canvas.style.height = `${windowHeight}px`
        canvas.style.top = `${(innerHeight - windowHeight) * .5}px`
        canvas.style.left = `${(innerWidth - windowWidth) * .5}px`
    }

    PIXI.interaction.InteractionManager.prototype.mapPositionToPoint = function(point, x, y) {
        let rect

        // IE 11 fix
        if (!this.interactionDOMElement.parentElement) {
            rect = {x: 0, y: 0, width: 0, height: 0}
        } else {
            rect = this.interactionDOMElement.getBoundingClientRect()
        }

        const resolutionMultiplier = navigator.isCocoonJS ? this.resolution : (1.0 / this.resolution)

        /*
        * 特殊处理: 强制横屏情况
        */
        if (canvas.style.transform === 'rotate(90deg)') {
            point.x = (y - rect.top) * (this.interactionDOMElement.width / rect.height) * resolutionMultiplier
            point.y = (1 - (x - rect.left) / rect.width) * this.interactionDOMElement.height * resolutionMultiplier
        } else {
            point.x = ((x - rect.left) * (this.interactionDOMElement.width / rect.width)) * resolutionMultiplier
            point.y = ((y - rect.top) * (this.interactionDOMElement.height / rect.height)) * resolutionMultiplier
        }
    }
}


config.screen.width = windowWidth * pixelRatio
config.screen.height = windowHeight * pixelRatio
config.screen.resolution = pixelRatio

if (config.design.mode === 'portrait' &&
    config.screen.ratio > 1) {
    config.zoom = Math.min(
        config.screen.height / config.design.width,
        config.screen.width / config.design.height
    )
} else {
    config.zoom = Math.min(
        config.screen.width / config.design.width,
        config.screen.height / config.design.height
    )
}

const app = new PIXI.Application({
    width: config.screen.width,
    height: config.screen.height,
    view: canvas,
    transparent: true,
    roundPixels: true,
    sharedLoader: true
})

// app.stage.scale.set(config.zoom)

export default app
export {default as Tilemap} from './tilemap'
export {default as Layout} from './layout'
export const monitor = new PIXI.utils.EventEmitter()
