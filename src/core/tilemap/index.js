import Layer from './layer'
import core from '../index'

const symbol = {
    map: Symbol('map')
}

export default class Tilemap extends PIXI.Container {
    layer = {}

    constructor(map, tilesheet) {
        super()
        this[symbol.map] = map
        this.tilesheet = tilesheet
        this.tileSize = [map.tilewidth, map.tileheight]
        this.parse()
    }

    parse() {
        const map = this[symbol.map]
        map.layers.forEach(raw => {
            raw.type === 'tilelayer' && this.addLayer(raw)
        })
    }

    addLayer(raw) {
        this.layer[raw.name] = this.addChild(new Layer({
            raw,
            tilesheet: this.tilesheet,
            tileSize: this.tileSize,
            tileset: this[symbol.map].tilesets[0]
        }))
    }
}