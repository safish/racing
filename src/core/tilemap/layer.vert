precision highp float;

attribute vec2 aVertexPosition, aTextureCoord;
uniform mat3 projectionMatrix;
uniform vec2 uMapSize;
varying vec2 vTextureCoord, vOffset;

void main() {
    gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);
    vTextureCoord = aTextureCoord;
    vOffset = vTextureCoord * uMapSize;
}