import planck from 'planck-js'
import {getValue} from '../utils'

const
    world = planck.World(planck.Vec2(0, 5)),
    ptm = 32,
    step = 1 / ptm


loop()
function loop() {
    for (let body = world.getBodyList(); body; body = body.getNext()) {
        if (body.node) {
            const
                node = body.node,
                point = body.getPosition()

            node.position.set(point.x * ptm, point.y * ptm)
            node.rotation = body.getAngle()
        }
    }
    world.step(step)
    requestAnimationFrame(loop)
}

/*
* 碰撞回调
* 事件分发
*/
world.on('begin-contact', contact => {
    const
        bodyA = contact.getFixtureA().getBody(),
        bodyB = contact.getFixtureB().getBody()

    bodyA.collideFixture = contact.getFixtureA()
    bodyB.collideFixture = contact.getFixtureB()

    bodyA.collidable && bodyA.node.emit('begin-contact', bodyA, bodyB, contact)
    bodyB.collidable && bodyB.node.emit('begin-contact', bodyB, bodyA, contact)
})

world.on('end-contact', contact => {
    const
        bodyA = contact.getFixtureA().getBody(),
        bodyB = contact.getFixtureB().getBody()

    bodyA.collideFixture = contact.getFixtureA()
    bodyB.collideFixture = contact.getFixtureB()

    bodyA.collidable && bodyA.node.emit('end-contact', bodyA, bodyB, contact)
    bodyB.collidable && bodyB.node.emit('end-contact', bodyB, bodyA, contact)
})

export {
    world,
    ptm
}

PIXI.DisplayObject.prototype.addBody = function(option={}) {
    const
        point = this.getGlobalPosition(),
        body = world.createBody({
            type: getValue(option.type, planck.Body.DYNAMIC),
            position: planck.Vec2(point.x * step, point.y * step),
            ...option
        })

    body.node = this
    this.body = body

    return body
}

planck.Body.prototype.addBox = function(option={}) {
    const
        shape = planck.Box(
            option.width * .5 * step,
            option.height * .5 * step,
            option.center ? planck.Vec2(option.center.x * step, option.center.y * step) : null,
            option.angle
        ),

        def = {
            density: getValue(option.density, 1),
            ...getValue(option.def, {})
        }

    this.createFixture(shape, def)

    return this
}


planck.Body.prototype.addCircle = function(option={}) {
    const
        shape = option.center ?
            planck.Circle(
                planck.Vec2(option.center.x * step, option.center.y * step),
                option.r * step
            ) : planck.Circle(option.r * step),

        def = {
            density: getValue(option.density, 1),
            ...getValue(option.def, {})
        }
    this.createFixture(shape, def)
    return this
}

planck.Body.prototype.addPolygon = function(option={}) {
    const
        shape = planck.Polygon(option.vertices.map(point =>
            planck.Vec2(point.x * step, point.y * step))),
        def = {
            density: getValue(option.density, 1),
            ...getValue(option.def, {})
        }
    this.createFixture(shape, def)
    return this
}

planck.Body.prototype.addChain = function(option={}) {
    const
        shape = planck.Chain(option.vertices.map(point => planck.Vec2(
            point.x * step, point.y * step)), option.loop),
        def = {
            density: getValue(option.density, 1),
            ...getValue(option.def, {})
        }

    this.createFixture(shape, def)
    return this
}

planck.Body.prototype.syncPosition = function(x, y) {
    x *= step
    y *= step
    this.setPosition(planck.Vec2(x, y))
    return this
}