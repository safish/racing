import {monitor} from './core'
import {game, prepare} from './scenes'

prepare.preload()
monitor.on('scene:game', game.create.bind(game))

