const
    path = require('path'),
    htmlWebpackPlugin = require('html-webpack-plugin'),
    webpack = require('webpack')

module.exports = {
    entry: [
        // 'pixi-sound',
        './src/app.js'
    ],

    output: {
        path: path.resolve('dist'),
        filename: 'game.js'
    },

    resolve: {
        alias: {
            PIXI: 'pixi.js'
            // core: path.resolve('./src/core')
        }
    },

    devServer: {
        hot: true,
        contentBase: '.'
    },

    devtool: 'source-map',

    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.(vert|frag)$/,
                use: ['raw-loader']
            }
        ]
    },

    plugins: [
        new htmlWebpackPlugin({
            template: './template.html',
            hash: true,
            filename: 'index.html',
            inject: 'body',
            minify: {
                collapseWhitespace: true
            }
        }),

        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),

        new webpack.DefinePlugin({
            web: JSON.stringify(true),
            stamp: JSON.stringify(Date.now())
        }),

        new webpack.ProvidePlugin({
            PIXI: 'PIXI'
        })
    ],

    mode: 'development'
}