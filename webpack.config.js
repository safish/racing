const
    path = require('path'),
    webpack = require('webpack')

const isProd = process.env.NODE_ENV === 'production' ? true : false

module.exports = {
    entry: [
        './libs/adapter/index.js',
        './src/app.js'
    ],

    output: {
        path: path.resolve('.'),
        filename: 'game.js'
    },

    resolve: {
        extensions: ['.ts', '.js'],
        alias: {
            PIXI: 'pixi.js'
        }
    },

    devtool: isProd ? false : 'source-map',

    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.(vert|frag)$/,
                use: ['raw-loader']
            }
        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            web: JSON.stringify(false),
            stamp: JSON.stringify(Date.now())
        }),

        new webpack.ProvidePlugin({
            PIXI: 'PIXI'
        })

    ],

    mode: isProd ? 'production' : 'development'
}